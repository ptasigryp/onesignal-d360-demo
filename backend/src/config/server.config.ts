export default () => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  accessKey: process.env.ACCESS_KEY,
});
