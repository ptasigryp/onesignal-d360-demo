import * as Joi from '@hapi/joi';

export default Joi.object({
  PORT: Joi.number().default(3000),
  ACCESS_KEY: Joi.string().required(),
  ONESIGNAL_APP_ID: Joi.string().required(),
  ONESIGNAL_API_KEY: Joi.string().required(),
});
