import { Module } from '@nestjs/common';
import { NotificationsModule } from 'src/services/notifications/notifications.module';
import DemoController from './demo.controller';
import DemoService from './demo.service';

@Module({
  imports: [NotificationsModule],
  controllers: [DemoController],
  providers: [DemoService],
  exports: [],
})
export class DemoModule {}
