import { Controller, Post, Body } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiBody } from '@nestjs/swagger';
import DemoService from './demo.service';
import CreateDemoNotificationDto from './dto/sendDemoNotification.dto';

@Controller('demo')
export default class DemoController {
  constructor(
    private readonly demoService: DemoService,
    private readonly configService: ConfigService,
  ) {}

  @ApiBody({ type: CreateDemoNotificationDto })
  @Post('push-notifications')
  async sendDemoPushNotificaitons(@Body() payload: CreateDemoNotificationDto) {
    if (payload.accessKey !== this.configService.get('accessKey')) {
      throw new Error('No permission');
    }

    await this.demoService.sendDemoPushNotification(
      payload.title,
      payload.subtitle,
      payload.message,
    );

    return {};
  }
}
