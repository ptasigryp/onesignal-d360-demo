import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class CreateDemoNotificationDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  accessKey: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  title: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  subtitle: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  message: string;
}

export default CreateDemoNotificationDto;
