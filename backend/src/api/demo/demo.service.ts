import { Injectable } from '@nestjs/common';
import NotificationsService from 'src/services/notifications/notifications.service';

@Injectable()
export default class DemoService {
  constructor(private readonly notificationsService: NotificationsService) {}

  async sendDemoPushNotification(
    title: string,
    subtitle: string,
    message: string,
  ) {
    return this.notificationsService.sendMessage(title, subtitle, message);
  }
}
