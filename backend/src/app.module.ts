import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import configValidator from './validators/config';
import serverConfiguration from './config/server.config';
import oneSignalConfig from './config/oneSignal.config';
import { NotificationsModule } from './services/notifications/notifications.module';
import { DemoModule } from './api/demo/demo.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: configValidator,
      load: [serverConfiguration, oneSignalConfig],
      isGlobal: true,
    }),
    NotificationsModule,
    DemoModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
