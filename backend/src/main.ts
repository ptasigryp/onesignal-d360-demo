import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const logger: Logger = new Logger('app');
  const app = await NestFactory.create(AppModule);

  const configService: ConfigService = app.get(ConfigService);
  const port: number = configService.get('port');

  const swaggerOptions = new DocumentBuilder()
    .setTitle('OneSignal D360 Push demo')
    .setDescription('REST API for demo')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, swaggerOptions);
  const documentPath = 'docs';
  SwaggerModule.setup(documentPath, app, document);
  logger.log(`* Using swagger at /${documentPath}`);

  await app.listen(port, () => {
    logger.log(`* Listening on ::${port}`);
  });
}
bootstrap();
