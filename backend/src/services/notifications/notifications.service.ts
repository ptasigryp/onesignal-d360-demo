import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AxiosRequestConfig } from 'axios';
import { lastValueFrom, map, tap } from 'rxjs';

@Injectable()
export default class NotificationsService {
  private readonly apiBaseURL = 'https://onesignal.com/api/v1';

  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
  ) {}

  async sendMessage(
    title: string,
    subtitle: string,
    message: string,
  ): Promise<void> {
    const data = {
      app_id: this.configService.get('oneSignal.appID'),
      headings: {
        en: title,
      },
      subtitle: {
        en: subtitle,
      },
      contents: {
        en: message,
      },
      included_segments: ['Subscribed Users'],
    };
    const config: AxiosRequestConfig = {
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        Authorization: 'Basic ' + this.configService.get('oneSignal.apiKey'),
      },
    };

    const result = await this._sendPostApiRequest(
      '/notifications',
      data,
      config,
    );
    console.log(result);
  }

  private _sendPostApiRequest(
    endpoint: string,
    data?: any,
    config?: AxiosRequestConfig<any>,
  ) {
    const url = this.apiBaseURL + endpoint;
    const source = this.httpService.post(url, data, config).pipe(
      tap((response) => console.log(response)),
      map((response) => response.data),
    );
    return lastValueFrom(source);
  }
}
