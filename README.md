# onesignal-d360-demo



## About

This is just a demonstration of basic OneSignal integration in iOS and backend (node.js). 

To run the iOS app just go to 'iOS' folder and open xcworkspace with xCode. Remember to install pods first using 'pod install'

To run the backend open 'backend' folder and open README file in it. 

## Requirements

- xCode
- node.js
- CocoaPods


## TestFlight

You can download the app on testflight here - https://testflight.apple.com/join/7oK0z0ZU

## Backend

You can find deployed backend here - https://onesignal-d-360-demo-guqyc.ondigitalocean.app/docs/