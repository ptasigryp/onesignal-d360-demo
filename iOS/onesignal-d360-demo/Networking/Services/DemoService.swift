//
//  DemoService.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 09/01/2022.
//

import Foundation
import Moya

enum DemoService {
    
    case pushNotification(accessKey: String, title: String, subtitle: String, message: String)
}

extension DemoService: TargetType {
    
    public var baseURL: URL {
        guard let url = URL(string: "\(Configuration.current.demoApiURL)/demo") else {
            fatalError("Base url not configured")
        }
        return url
    }
    
    public var path: String {
        switch self {
        case .pushNotification:
            return "/push-notifications"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .pushNotification:
            return .post
        }
    }
    
    public var task: Task {
        switch self {
        case .pushNotification(let accessKey, let title, let subtitle, let message):
            return .requestParameters(
                parameters: [
                    "accessKey": accessKey,
                    "title": title,
                    "subtitle": subtitle,
                    "message": message
                ],
                encoding: JSONEncoding.default
            )
        }
    }
    
    public var headers: [String : String]? {
       [
        "Content-Type": "application/json",
        "Accept": "application/json"
       ]
    }
    
    var validationType: ValidationType {
        .successCodes
    }
}
