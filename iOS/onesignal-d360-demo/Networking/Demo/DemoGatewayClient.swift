//
//  DemoGatewayClient.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 09/01/2022.
//

import Foundation

public protocol DemoGatewayClient {
    
    func requestDemoPushNotification(
        title: String,
        subtitle: String,
        message: String,
        completion: @escaping EmptyNetworkHandler
    )
}

extension GatewayClient: DemoGatewayClient {
    
    public func requestDemoPushNotification(
        title: String,
        subtitle: String,
        message: String,
        completion: @escaping EmptyNetworkHandler
    ) {
        request(
            provider: demoProvider,
            target: .pushNotification(
                accessKey: apiDemoAccessKey,
                title: title,
                subtitle: subtitle,
                message: message
            ),
            responseType: EmptyResponse.self
        ) { result in
            switch result {
            case .success:
                completion(.success(()))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
