//
//  GatewayClient.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 09/01/2022.
//

import Foundation
import Moya

public class GatewayClient {
    
    let demoProvider = ProviderBuilder.builder.provider(forTarget: DemoService.self)
    let apiDemoAccessKey: String

    public init(
        apiDemoAccessKey: String = Configuration.current.demoAccessKey
    ) {
        self.apiDemoAccessKey = apiDemoAccessKey
    }
    

    @discardableResult
    internal func request<ResponseType: Decodable, Target: TargetType>(
        provider: MoyaProvider<Target>,
        target: Target,
        responseType: ResponseType.Type,
        completion: @escaping NetworkHandler<ResponseType>
    ) -> Moya.Cancellable {
        return provider.request(target) { [weak self] result in
            switch result {
            case .success(let response):
                self?.parseResponse(response: response, responseType: responseType, completion: completion)
            case .failure(let moyaError):
                completion(.failure(moyaError))
            }
        }
    }

    internal func parseResponse<ResponseType: Decodable>(
        response: Response,
        responseType: ResponseType.Type,
        completion: NetworkHandler<ResponseType>
    ) {
        do {
            let response = try response.map(ResponseType.self)
            completion(.success(response))
        } catch {
            print("Error decoding response. Error: \(error)")
            completion(.failure(error))
        }
    }
}
