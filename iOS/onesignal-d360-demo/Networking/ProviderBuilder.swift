//
//  ProviderBuilder.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 09/01/2022.
//

import Foundation
import Moya

class ProviderBuilder {
    
    static let builder = ProviderBuilder()
    
    func provider<T: TargetType>(
        forTarget target: T.Type
    ) -> MoyaProvider<T> {
        return MoyaProvider<T>(
            session: Moya.Session()
        )
    }
}
