//
//  WelcomeViewModel.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 06/01/2022.
//

import Foundation

final class WelcomeViewModel {
    
    private let notificationsModel: NotificationsModel
    
    init(notificationsModel: NotificationsModel = RestNotificationsModel()) {
        self.notificationsModel = notificationsModel
    }
    
    func didLoad() {
        notificationsModel.requestPushNotificationsPermissionsIfNeeded()
    }
    
    func requestPushTapped() {
        notificationsModel.requestWelcomePushNotification { result in
            switch result {
            case .success:
                print("Success request")
            case .failure(let error):
                print("Error: \(error)")
            }
        }
    }
}

