//
//  WelcomeViewController.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 06/01/2022.
//

import UIKit

protocol WelcomeViewControllerDelegate: AnyObject {
    
    func welcomeViewController(didTapCustomPushButton controller: WelcomeViewController)
}

final class WelcomeViewController: UIViewController, NibLoadable {
    
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var pushButton: UIButton!
    @IBOutlet private var customPushButton: UIButton!
    
    weak var delegate: WelcomeViewControllerDelegate?
    private let viewModel: WelcomeViewModel
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        viewModel.didLoad()
    }
    
    init(viewModel: WelcomeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: Self.self), bundle: Bundle(for: Self.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        titleLabel.text = "Welcome to D360 Push Notifications Demo"
        descriptionLabel.text = "Made by Michał Mańkus for testing purposes. Tap the button below to request REST Api to send you a test push notification."
        pushButton.setTitle("Request push", for: .normal)
        pushButton.addTarget(self, action: #selector(pushButtonTapped), for: .touchUpInside)
        customPushButton.setTitle("Custom message", for: .normal)
        customPushButton.addTarget(self, action: #selector(customPushButtonTapped), for: .touchUpInside)
    }
    
    @objc private func pushButtonTapped() {
        viewModel.requestPushTapped()
    }
    
    @objc private func customPushButtonTapped() {
        delegate?.welcomeViewController(didTapCustomPushButton: self)
    }
}
