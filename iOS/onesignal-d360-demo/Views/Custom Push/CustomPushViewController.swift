//
//  CustomPushViewController.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 09/01/2022.
//

import UIKit

final class CustomPushViewController: UIViewController, NibLoadable {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var messageTextField: UITextField!
    @IBOutlet var pushButton: UIButton!
    
    private let viewModel: CustomPushViewModel
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        messageTextField.becomeFirstResponder()
    }
    
    init(viewModel: CustomPushViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: Self.self), bundle: Bundle(for: Self.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        titleLabel.text = "Type message you want to send"
        pushButton.setTitle("Request push notification", for: .normal)
        pushButton.addTarget(self, action: #selector(pushButtonTapped), for: .touchUpInside)
    }
    
    @objc private func pushButtonTapped() {
        guard let text = messageTextField.text, text.isEmpty == false else { return }
        viewModel.requestPushTapped(message: text)
    }
}
