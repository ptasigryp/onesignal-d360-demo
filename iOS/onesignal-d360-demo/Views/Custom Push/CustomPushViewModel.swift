//
//  CustomPushViewModel.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 09/01/2022.
//

import Foundation

final class CustomPushViewModel {
    
    private let notificationsModel: NotificationsModel
    
    init(notificationsModel: NotificationsModel = RestNotificationsModel()) {
        self.notificationsModel = notificationsModel
    }
    
    func requestPushTapped(message: String) {
        notificationsModel.requestPushNotification(message: message) { result in
            switch result {
            case .success:
                print("Success request")
            case .failure(let error):
                print("Error: \(error)")
            }
        }
    }
}
