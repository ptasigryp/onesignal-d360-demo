//
//  RestNotificationsModel.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 09/01/2022.
//

import Foundation

public class RestNotificationsModel: NotificationsModel {
    
    private let demoGateway: DemoGatewayClient
    private let notificationsManager: NotificationsManagerProtocol
    
    public init(
        demoGateway: DemoGatewayClient = GatewayClient(),
        notificationsManager: NotificationsManagerProtocol = NotificationsManager.shared
    ) {
        self.demoGateway = demoGateway
        self.notificationsManager = notificationsManager
    }
    
    public func requestPushNotificationsPermissionsIfNeeded() {
        notificationsManager.requestPushNotificationsPermissionsIfNeeded()
    }
    
    public func requestWelcomePushNotification(completion: @escaping EmptyNetworkHandler) {
        demoGateway.requestDemoPushNotification(
            title: "Welcome",
            subtitle: "This is just a test",
            message: "Demo for push notifications using OneSignal made for Ali by M. Mankus :)",
            completion: completion
        )
    }
    
    public func requestPushNotification(message: String, completion: @escaping EmptyNetworkHandler) {
        demoGateway.requestDemoPushNotification(
            title: "Test notification",
            subtitle: "This is custom test notification",
            message: message,
            completion: completion
        )
    }
}
