//
//  NotificationModel.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 06/01/2022.
//

import Foundation

public protocol NotificationsModel {
    
    func requestPushNotificationsPermissionsIfNeeded()
    func requestWelcomePushNotification(completion: @escaping EmptyNetworkHandler)
    func requestPushNotification(message: String, completion: @escaping EmptyNetworkHandler)
}
