//
//  MockNotificationModel.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 06/01/2022.
//

import Foundation

public class MockNotificationsModel: NotificationsModel {
    
    public init() {}
    
    public func requestPushNotificationsPermissionsIfNeeded() {
        print("request sent")
    }
    
    public func requestWelcomePushNotification(completion: @escaping EmptyNetworkHandler) {
        completion(.success(()))
    }
    
    public func requestPushNotification(message: String, completion: @escaping EmptyNetworkHandler) {
        completion(.success(()))
    }
}
