//
//  AppCoordinator.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 06/01/2022.
//

import Foundation
import UIKit

final class AppCoordinator: Coordinator {
    
    private let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        let viewModel = WelcomeViewModel()
        let viewController = WelcomeViewController(viewModel: viewModel)
        viewController.delegate = self
        window.rootViewController = viewController
        window.makeKeyAndVisible()
    }
}

extension AppCoordinator: WelcomeViewControllerDelegate {
    
    func welcomeViewController(didTapCustomPushButton controller: WelcomeViewController) {
        let viewModel = CustomPushViewModel()
        let viewController = CustomPushViewController(viewModel: viewModel)
        controller.present(viewController, animated: true, completion: nil)
    }
}
