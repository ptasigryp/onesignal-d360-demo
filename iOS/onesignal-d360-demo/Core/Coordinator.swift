//
//  Coordinator.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 06/01/2022.
//

import Foundation

protocol Coordinator {
    
    func start()
}
