//
//  NibLoadable.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 06/01/2022.
//

import UIKit

protocol NibLoadable: AnyObject {
    
    static var nib: UINib { get }
}

extension NibLoadable {

    static var nib: UINib {
        UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
}

extension NibLoadable where Self: UIViewController {
    
    static func instantiate() -> Self {
        self.init(nibName: String(describing: self), bundle: Bundle(for: self))
    }
}
