//
//  Configuration.swift
//  Core
//
//  Created by Ahmed Abdurrahman on 10/26/20.
//

import Foundation
import UIKit

public enum Configuration: String {

    // MARK: - configurations

    case debugDevelopment = "Debug"
    case releaseDevelopment = "Release.Dev"

    case debugStaging = "Debug.Staging"
    case releaseStaging = "Release.Staging"

    case debugProduction = "Debug.Prod"
    case releaseProduction = "Release.Prod"

    public static let current: Configuration = {
        return .debugDevelopment // JUST FOR TESTING
    }()
}

// MARK: - Gateway

extension Configuration {

    public var oneSignalAppId: String {
        switch self {
        case .debugDevelopment, .releaseDevelopment:
            return "fffe95c6-aca8-4084-b922-5d1ddc60e7b1"
        case .debugStaging, .releaseStaging:
            return "fffe95c6-aca8-4084-b922-5d1ddc60e7b1"
        case .debugProduction, .releaseProduction:
            return "fffe95c6-aca8-4084-b922-5d1ddc60e7b1"
        }
    }
    
    public var demoAccessKey: String {
        switch self {
        case .debugDevelopment, .releaseDevelopment:
            return "8NWa4EcLvpTyDJKmDkq3BhR4E9CpZDyR"
        case .debugStaging, .releaseStaging:
            return "8NWa4EcLvpTyDJKmDkq3BhR4E9CpZDyR"
        case .debugProduction, .releaseProduction:
            return "8NWa4EcLvpTyDJKmDkq3BhR4E9CpZDyR"
        }
    }
    
    public var demoApiURL: String {
        switch self {
        case .debugDevelopment, .releaseDevelopment:
            return "https://onesignal-d-360-demo-guqyc.ondigitalocean.app"
        case .debugStaging, .releaseStaging:
            return "https://onesignal-d-360-demo-guqyc.ondigitalocean.app"
        case .debugProduction, .releaseProduction:
            return "https://onesignal-d-360-demo-guqyc.ondigitalocean.app"
        }
    }
}
