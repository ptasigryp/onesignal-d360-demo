//
//  LaunchManager.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 06/01/2022.
//

import Foundation
import UIKit

final class LaunchManager {
    
    static let shared = LaunchManager()
    
    func launch(_ application: UIApplication, launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        let configuration = Configuration.current
        
        NotificationsManager.shared.launch(
            application: application,
            launchOptions: launchOptions,
            oneSignalAppId: configuration.oneSignalAppId
        )
    }
}
