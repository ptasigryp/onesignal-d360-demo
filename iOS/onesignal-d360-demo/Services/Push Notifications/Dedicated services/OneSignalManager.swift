//
//  OneSignalManager.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 06/01/2022.
//

import Foundation
import UIKit
import OneSignal

public protocol OneSignalManagerProtocol {
    
    func launch(
        application: UIApplication,
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?,
        appId: String
    )
    func requestPushNotificationsPermissionsIfNeeded()
}

public class OneSignalManager: OneSignalManagerProtocol {
    
    public static let shared = OneSignalManager()

    public init() {}
    
    public func launch(
        application: UIApplication,
        launchOptions: [UIApplication.LaunchOptionsKey : Any]?,
        appId: String
    ) {
        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        OneSignal.initWithLaunchOptions(launchOptions)
        OneSignal.setAppId(appId)
    }
    
    public func requestPushNotificationsPermissionsIfNeeded() {
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
    }
}
