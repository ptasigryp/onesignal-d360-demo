//
//  NotificationsManager.swift
//  onesignal-d360-demo
//
//  Created by Michał Mańkus on 06/01/2022.
//

import Foundation
import UIKit

public protocol NotificationsManagerProtocol {

    func requestPushNotificationsPermissionsIfNeeded()
}

public class NotificationsManager: NotificationsManagerProtocol {
    
    public static let shared = NotificationsManager()
    
    private let oneSignalManager: OneSignalManagerProtocol

    public init(
        oneSignalManager: OneSignalManagerProtocol = OneSignalManager.shared
    ) {
        self.oneSignalManager = oneSignalManager
    }
    
    func launch(
        application: UIApplication,
        launchOptions: [UIApplication.LaunchOptionsKey : Any]?,
        oneSignalAppId: String
    ) {
        oneSignalManager.launch(
            application: application,
            launchOptions: launchOptions,
            appId: oneSignalAppId
        )
        // TODO: - Launch other managers if needed (firebase etc)
    }
    
    public func requestPushNotificationsPermissionsIfNeeded() {
        oneSignalManager.requestPushNotificationsPermissionsIfNeeded()
    }
}
